<!--
Hey, thanks for using the awesome-readme-template template.  
If you have any enhancements, then fork this project and create a pull request 
or just open an issue with the label "enhancement".

Don't forget to give this project a star for additional support ;)
Maybe you can mention me or this repo in the acknowledgements too
-->
<div align="center">
  <h1> :snake: iGrab :snake:</h1>
  
  <p>
    <strong>I</strong>'m gonna go <strong>grab</strong> some testcase data == <strong> igrab </strong>
  </p>

  <p>
  igrab is a command-line python tool to make the retrieval of testing results easier for kernel maintainers.
  Currently, a standard workflow for a kernel maintainer is to translate from dist git --> brew --> retrieve nvr/taskID --> OSCI --> open tabs from CKIDB/OSCI artifacts. It is also necessary to confirm you are investigating the correct build(s), identify testcase names and investigate build logs, console logs, task logs and test logs.
  
  igrab achieves the same workflow with the execution of <code> igrab -n $NVR </code> which prints out the testcase names, their OSCI "satisfaction" status, and endpoint references so that you always know what you're looking at, all the time.
  </p>
  
  
<!-- Badges -->
<p>
  <a href="https://gitlab.com/debarbos/igrab/-/graphs/main">
    <img src="https://img.shields.io/gitlab/contributors/debarbos/igrab" alt="contributors" />
  </a>
  <a href="https://gitlab.com/debarbos/igrab/-/commits/main">
    <img src="https://img.shields.io/gitlab/last-commit/debarbos/igrab" alt="last commit"/>
  </a>
  <a href="https://gitlab.com/debarbos/igrab/-/forks">
    <img src="https://img.shields.io/gitlab/forks/debarbos/igrab" alt="forks" />
  </a>
  <a href="https://gitlab.com/debarbos/igrab/-/issues">
    <img src="https://img.shields.io/gitlab/issues/open-raw/debarbos/igrab" alt="open issues" />
  </a>
  <a href="https://gitlab.com/debarbos/igrab/-/blob/main/LICENSE.md">
    <img src="https://img.shields.io/gitlab/license/debarbos/igrab" alt="license" />
  </a>
</p>
   
<h4>
    <a href="https://gitlab.com/debarbos/igrab/-/blob/main/README.md">Documentation</a>
  <span> · </span>
    <a href="https://gitlab.com/debarbos/igrab/-/issues">Report Bug</a>
  <span> · </span>
    <a href="https://gitlab.com/debarbos/igrab/-/merge_requests">Open an MR</a>
  </h4>
</div>

<br />

<!-- Table of Contents -->
# :notebook_with_decorative_cover: Table of Contents

- [:notebook\_with\_decorative\_cover: Table of Contents](#notebook_with_decorative_cover-table-of-contents)
  - [:star2: About the Project](#star2-about-the-project)
    - [:camera: Screenshots](#camera-screenshots)
    - [:dart: Features](#dart-features)
  - [:smile: Getting Started](#smile-getting-started)
    - [:bangbang: Prerequisites](#bangbang-prerequisites)
    - [:runner: Run Locally](#runner-run-locally)
  - [:dvd: Installation](#dvd-installation)
  - [:computer: Usage](#computer-usage)
  - [:scroll: Roadmap](#scroll-roadmap)
  - [:wave: Contributing](#wave-contributing)
  - [:question: FAQ](#question-faq)
  - [:pencil2: License](#pencil2-license)
  - [:handshake: Contact](#handshake-contact)
  - [:gem: Acknowledgements](#gem-acknowledgements)

  

<!-- About the Project -->
## :star2: About the Project


<!-- Screenshots -->
### :camera: Screenshots

<div align="center"> 
  <img src="./etc/igrab-demo.gif" alt="script demo" />
</div>

<!-- Features -->
### :dart: Features

- Retrieval of Brew Build Status/Metadata
- Collection of OSCI dashboard data (ResultsDB, Datawarehouse, CKI Tests)
- Retrieval of Test Result source (API endpoints, URL collections)
- More to come soon :)

Have a suggestion? Open an issue :)

<!-- Getting Started -->
## :smile: Getting Started

<!-- Prerequisites -->
### :bangbang: Prerequisites

This project uses pip as a package manager

Ensure your version of pip is up-to-date.

Tested/Developed on Python 3.10, with little testing done on 3.6.8

```bash
    python -m pip install -–upgrade pip
```

<!-- Run Locally -->
### :runner: Run Locally

Clone the project via HTTPS:

```bash
    git clone https://gitlab.com/debarbos/igrab
```

Alternatively, clone it via SSH:

```bash
    git clone git@gitlab.com:debarbos/igrab.git
```

(OPTIONAL) Set up a Python Virtual Environment (VENV) for Dependency Management

```bash
    mkdir pyvenv
    python3 -m venv pyvenv/
```

Activate your new Python Venv

```bash
    source pyvenv/bin/activate
```

Go to the project directory

```bash
    cd igrab/
```

Install dependencies

```bash
    pip install -r requirements.txt
```

Go to the Source Directory
```bash
    cd src/
```

<!-- Installation -->

## :dvd: Installation

Rather than requiring the source file at hand and requiring to call python from the terminal, you can install this tool using the `setup.sh` script, which will check your local python version, install `requirements.txt`, and copy `igrab.py` to your `$HOME/.local/bin`.

To run the setup script:
```bash
bash setup.sh
```
Usage will be identical to the [Usage](#computer-usage) section, but with the `python` keyword omitted.

<!-- Usage -->
## :computer: Usage

Simply put, this tool allows other Kernel Maintainers to retrieve Brew Build statuses, test results (from OSCI Dashboard) and test result sources.

To use this tool, you must first have a Brew TASK ID (not a build ID!) or a Build NVR

NOTE: If multiple builds the same NVR, the tool may have some difficulty collecting test results.

```bash
    python igrab.py --id ${TASK_ID} OR --nvr ${NVR}
```

Need help? Use:

```bash
    python igrab.py --help
```

<!-- Roadmap -->
## :scroll: Roadmap

* [x] Implement the Koji API and remove Brew Subprocs
* [x] Refactor OSCI retrival to utilize GraphQL endpoint
* [x] Grab ResultsDB data for failed test results
* [x] Display testcase source data for optional inspection
* [x] Implement a Verbose print option to reduce clutter for simple requests.
* [x] Implement a save-to-file option
* [x] Refactor parsing logic to no longer require asynchronous requests 
* [x] Overhaul argument parsing to allow for greater tool flexibility.

<!-- Contributing -->
## :wave: Contributing

<a href="https://gitlab.com/debarbos/igrab/-/graphs/main">
  <img src="https://gitlab.com/uploads/-/system/user/avatar/12108941/avatar.png?width=96" />
</a>


Contributions are always welcome!

Please email one of the [Contacts](#handshake-contact) to get started.


<!-- FAQ -->
## :question: FAQ

- When I try to run this script, I get an SSL Error, what gives?

  + Long story short, this is a weird certificate issue caused Brewhub/Brewweb/Brew. If you have updated Red Hat Security Certifications located on your machine (also referred to as CAs, IT Certs, etc) adding the following line to your $SHELL profile/init/rc script may correct the issue: `export REQUESTS_CA_BUNDLE=/etc/pki/tls/certs/ca-bundle.crt`. Please refer to the issue tracker for a more "in depth" description and resolution of the [issue](https://gitlab.com/debarbos/igrab/-/issues/1).

- I'm missing a krb5.conf file
  + This is most likely due to a missing krb5-devel package on your machine. Install it using your package manager and it should be generated. Make sure you `kdestroy -A` then `kinit` in the terminal.

<!-- License -->
## :pencil2: License

Distributed under the Apache 2.0 License. Pease See [LICENSE.md](./LICENSE.md) for more information.


<!-- Contact -->
## :handshake: Contact

Derek Barbosa - debarbos (AT) redhat (DOT) com

<!-- Acknowledgments -->
## :gem: Acknowledgements

Shoutout to these amazing projects!

 - [Shields.io](https://shields.io/)
 - [Awesome README](https://github.com/matiassingers/awesome-readme)
 - [Emoji Cheat Sheet](https://github.com/ikatyang/emoji-cheat-sheet/blob/master/README.md#travel--places)
 - [Readme Template](https://github.com/Louis3797/awesome-readme-template)
 - Shoutout to Jan Stancek (jstancek AT redhat DOT com) for pointing to the OSCI GraphQL endpoint
